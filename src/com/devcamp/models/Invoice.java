package com.devcamp.models;

public class Invoice {
 /**
  * Phương thức
  */
 private int id;
 private Customer Customer;
 private double amount;

 public Invoice( int id, Customer Customer, double amount) {
  this.id = id;
  this.Customer = Customer;
  this.amount = amount;

 }
 /**
  * Geeter method
  * @return
  */

  public int getId() {
   return id;
  }

  public Customer getCustomer() {
   return Customer;
  }

  public int getCustomerID() {
   return Customer.getId();
  }

  public String getCustomerName() {
   return Customer.getName();
  }

  public int getCustomerDiscount() {
   return Customer.getDiscount();
  }

  public int getCustomerAfterDiscount() {
  double AmountCal = this.amount *  ((double)(Customer.getDiscount())/100);
  return (int)(this.amount - AmountCal);
  }

  @Override
  public String toString() {
   return "Invoice[id="+ id+",customer="+ this.Customer +",amount="+ amount+"]";
  }
}
