import com.devcamp.models.Customer;
import com.devcamp.models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1,"Ngoc Hoa",20);
        System.out.println("Customer1:");
        System.out.println(customer1);

        Customer customer2 = new Customer(2,"Khanh Son",30);
        System.out.println("Customer2:");
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(3,customer1,40000);
        System.out.println("Invoice1:");
        System.out.println(invoice1.getCustomerAfterDiscount());
        System.out.println(invoice1);

        Invoice invoice2 = new Invoice(4,customer2,50000);
        System.out.println("Invoice2:");
        System.out.println(invoice2);
    }
}
